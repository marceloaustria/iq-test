Rails.application.routes.draw do
    


  match 'user-test',  to: 'user_test#index', via: [:get, :post], as: :user_test_search
  get 'user-test/new', to: 'user_test#new'
  post 'user-test/create', to: 'user_test#create'

  get 'user-test/search_user', to: 'user_test#search_user'

  match 'analytics',  to: 'analytics#index', via: [:get, :post], as: :analytic_search
  get 'analytics/view/:id', to: 'analytics#show'
  post 'analytics/retake/:id', to: 'analytics#retake', as: :retake_user_analytic
  post 'analytics/delete/:id', to: 'analytics#delete', as: :delete_user_analytic

  get 'dashboard', to: "users#dashboard", as: :dashboard
  match 'user-candidates',  to: 'users#index', via: [:get, :post], as: :user_search
  get  'users/new',  to: 'users#new'
  post  'users/delete/:id',  to: 'users#delete', as: :delete_user
  get  'users/edit/:id',  to: 'users#edit'
  get  'users/invite/:id',  to: 'users#edit'
  patch 'users/update',  to: 'users#update', as: :update_user
  post 'users/create',  to: 'users#add_user'
  # post 'users/search', to: 'users#search', as: :user_search


 
  get 'result', to: 'take_test#result'
  get 'take-test/:id', to: 'take_test#take_test'
  post 'submit-test', to: "take_test#submit_test", as: :submit_test
  get 'profile', to: "take_test#profile", as: :profile
  post 'update-profile',  to: 'take_test#update_profile', as: :update_profile
  # devise_scope :admin do
  #   match '/admins/sign_out' => 'devise/sessions#destroy', via: [:get, :delete]
  #   # get '/', to: 'devise/sessions#new'
  # end

  # devise_for :admins

  # devise_for :admins, path: 'auth', path_names: { sign_in: 'login' }

  devise_scope :user do
    match '/users/sign_out' => 'devise/sessions#destroy', via: [:get, :delete]
  end


  # devise_for :users

  devise_for :users, :controllers => { :registrations => 'member_registrations' }

  get 'test-library/preview-test/:id', to: 'tests#preview_test'

  # get 'test-library', to: 'tests#index'

  match 'test-library',  to: 'tests#index', via: [:get, :post], as: :test_search

  post 'test-library/delete/:id', to: 'tests#delete_test', as: :delete_test_index


  get 'tests/new/:id', to: "tests#new", as: :new_test

  delete 'tests/delete/:id', to: "tests#delete", as: :delete_test

  post 'tests/update_status/:id', to: "tests#update_status"

  post 'tests/create', to: "tests#create"

  post 'tests/create_test_question', to: "tests#create_test_question"




  mount Ckeditor::Engine => '/ckeditor'
  # get 'questions', to: 'questions#index' 
  # get 'questions', to: 'questions#index'
  # post 'questions/search', to: 'questions#search', as: :question_search
  match 'questions',  to: 'questions#index', via: [:get, :post], as: :question_search
  get 'questions/new', to: 'questions#multiple_choice'
  post 'questions/delete/:id', to: 'questions#delete_multiple_choice', as: :delete_question
  get 'questions/edit/:id', to: 'questions#edit_multiple_choice', as: :edit_question
  post 'questions/update', to: 'questions#update_multiple_choice', as: :update_question
  # get 'results', to: 'results#index'
   match 'results',  to: 'results#index', via: [:get, :post], as: :result_search
  post 'results/create', to: 'results#result_create'
  post 'results/delete/:id', to: 'results#delete', as: :delete_result
  get 'results/edit/:id', to: 'results#edit', as: :edit_result
  post 'results/update', to: 'results#update', as: :update_result

  post 'questions_multiple_choice/create', to: 'questions#multiple_choice_create'
  post 'questions-section/create', to: 'questions#question_section_create'


  match 'print',  to: 'print_report#index', via: [:get, :post], as: :print_search
  get 'print/report', to: 'print_report#print_report'
  get 'print/single-report', to: 'print_report#print_single_report'
  # question sections
  match 'question-sections',  to: 'question_sections#index', via: [:get, :post], as: :question_sections_search
  post 'question-sections/create', to: 'question_sections#result_create'
  post 'question-sections/delete/:id', to: 'question_sections#delete', as: :delete_question_sections
  get 'question-sections/edit/:id', to: 'question_sections#edit', as: :edit_question_sections
  post 'question-sections/update', to: 'question_sections#update', as: :update_question_sections


  # years
  match 'years',  to: 'years#index', via: [:get, :post], as: :years_search
  post 'years/create', to: 'years#result_create'
  post 'years/delete/:id', to: 'years#delete', as: :delete_years
  get 'years/edit/:id', to: 'years#edit', as: :edit_years
  post 'years/update', to: 'years#update', as: :update_years

 # courses
  match 'courses',  to: 'courses#index', via: [:get, :post], as: :courses_search
  post 'courses/create', to: 'courses#result_create'
  post 'courses/delete/:id', to: 'courses#delete', as: :delete_courses
  get 'courses/edit/:id', to: 'courses#edit', as: :edit_courses
  post 'courses/update', to: 'courses#update', as: :update_courses

 # section
  match 'sections',  to: 'sections#index', via: [:get, :post], as: :sections_search
  post 'sections/create', to: 'sections#result_create'
  post 'sections/delete/:id', to: 'sections#delete', as: :delete_sections
  get 'sections/edit/:id', to: 'sections#edit', as: :edit_sections
  post 'sections/update', to: 'sections#update', as: :update_sections



  # get 'user-candidates', to: 'users#index'
   get 'settings', to: 'users#setting'

  get 'myquestions/index'

  get 'myquestions/create'

  get 'myquestions/update'

  get 'myquestions/delete'


  get 'terms-conditions', to: 'home#terms_conditions'

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  root 'home#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
