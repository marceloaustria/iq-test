class MemberRegistrationsController < Devise::RegistrationsController

# POST /resource
  def create
    build_resource(sign_up_params)

    resource.role = "member"
    resource.save

 		if params[:user_prepare_test].present?  
            prepare_test = params[:user_prepare_test].reject { |q| q[:test_id].blank?}
            if prepare_test.present?
                prepare_test.each_with_index do |p, index|      
                  qa = UserPrepareTest.new()
                  qa.user_id  = resource.id
                  qa.test_id = p[:test_id] 
                  qa.status = 'take'
                  if qa.save
                  end
                end
            end
        end  



    yield resource if block_given?
    if resource.persisted?
      if resource.active_for_authentication?
        set_flash_message! :notice, :signed_up
        sign_up(resource_name, resource)
        respond_with resource, location: after_sign_up_path_for(resource)
      else
        set_flash_message! :notice, :"signed_up_but_#{resource.inactive_message}"
        expire_data_after_sign_in!
        respond_with resource, location: after_inactive_sign_up_path_for(resource)
      end
    else
      clean_up_passwords resource
      set_minimum_password_length
      respond_with resource
    end
end

end