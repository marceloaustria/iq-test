class UserTestController < ApplicationController

 before_action :authenticate_user!
 before_action :authenticate_role
  layout 'admin_lte_2'


  def index
  	@page_header = "Manage User Test"
  	@page_desc = "Prepare Test"

    if params[:title].blank?
    @analytics = UserPrepareTest.paginate_by_sql("        
          select 
          upt.id, u.avatar, upt.user_id, u.first_name, u.last_name, u.email, u.last_sign_in_ip,
           t.test_name, upt.test_id, upt.created_at 
          from user_prepare_tests upt
          inner join users u on u.id = upt.user_id
          inner join tests t on t.id = upt.test_id
          order by upt.created_at desc
      ", :page => params[:page], :per_page => 15) 
    else
          @analytics = UserPrepareTest.paginate_by_sql("        
          select 
          upt.id, u.avatar, upt.user_id, u.first_name, u.last_name, u.email, u.last_sign_in_ip, 
          t.test_name,  upt.test_id, upt.created_at
          from user_prepare_tests upt
          inner join users u on u.id = upt.user_id
          inner join tests t on t.id = upt.test_id
          where lower(u.first_name) like '%#{params[:title].downcase}%' or lower(u.last_name) like '%#{params[:title].downcase}%' or lower(u.email) like '%#{params[:title].downcase}%'
          order by upt.created_at desc
      ", :page => params[:page], :per_page => 15) 
    end
  end

  def show
  	u = User.find(params[:user_id])
    t = Test.find(params[:test_id])
    @page_header = u.first_name + " " + u.last_name
    @page_desc =  t.test_name
    @report = UserTest.get_report(params[:test_id], params[:user_id])

    @get_result_id = UserTest.find_by_sql(["SELECT user_tests.question_answer_id, COUNT(*) AS ct,
      (select result_id from question_answers where id = user_tests.question_answer_id )as answer_result
FROM user_tests
Inner join user_prepare_tests on user_prepare_tests.id = user_tests.user_prepare_test_id
WHERE user_prepare_tests.user_id = ? and user_prepare_tests.test_id = ? and question_answer_id is not null
GROUP BY  answer_result
ORDER BY ct desc
",params[:user_id], params[:test_id]] )
    if @get_result_id.present?
      @result = Result.find(@get_result_id.first.answer_result)
    end
  end

  def delete
    @analytics_del =  UserPrepareTest.find(params[:id])
    @user_test =  UserTest.where(user_prepare_test_id: params[:id])
    @analytics_del.destroy
    @user_test.destroy_all if @user_test.present?
    
    @analytics = UserPrepareTest.paginate_by_sql("        
          select 
          upt.id, u.first_name, u.last_name, u.email, u.last_sign_in_ip, upt.time_start, upt.time_end,
          ( select created_at from user_tests where user_prepare_test_id = upt.id limit 1  )as created_at,
          u.avatar, t.test_name, upt.status, r.title, upt.user_id, upt.test_id
          from user_prepare_tests upt
          inner join users u on u.id = upt.user_id
          inner join tests t on t.id = upt.test_id
          left join results r on r.id = upt.result_id
           order by created_at desc
      ", :page => params[:page], :per_page => 15) 
    
    

    render :layout => false
  end

  def retake
    @analytics_retake =  UserPrepareTest.find(params[:id])
    @user_test =  UserTest.where(user_prepare_test_id: params[:id])
    @analytics_retake.update_attributes(status: "retake", result_id: nil)
    @user_test.destroy_all if @user_test.present?
    
    @analytics = UserPrepareTest.paginate_by_sql("        
          select 
          upt.id, u.first_name, u.last_name, u.email, u.last_sign_in_ip, upt.time_start, upt.time_end,
          ( select created_at from user_tests where user_prepare_test_id = upt.id limit 1  )as created_at,
          u.avatar, t.test_name, upt.status, r.title, upt.user_id, upt.test_id
          from user_prepare_tests upt
          inner join users u on u.id = upt.user_id
          inner join tests t on t.id = upt.test_id
          left join results r on r.id = upt.result_id
           order by created_at desc
      ", :page => params[:page], :per_page => 15) 
    

    render :layout => false
  end
end
