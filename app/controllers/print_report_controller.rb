class PrintReportController < ApplicationController

	def index

	    @page_header = "Print Report"
	  	@page_desc = "View Reports"
	  	session[:title] = params[:title]
	    if params[:title].blank?
	    @print = UserPrepareTest.paginate_by_sql("        
	          select 
	          upt.id, u.avatar, c.course_name, y.year_name, s.section_name,  upt.user_id, u.first_name, u.last_name, u.email, u.last_sign_in_ip, upt.time_start, upt.time_end,
	          ( select created_at from user_tests where user_prepare_test_id = upt.id limit 1  )as created_at,
	           t.test_name, upt.status, r.title, upt.test_id
	          from user_prepare_tests upt
	          inner join users u on u.id = upt.user_id
	          inner join tests t on t.id = upt.test_id
	          inner join courses c on c.id = u.course_id
	          inner join years y on y.id = u.year_id
	          inner join sections s on s.id = u.section_id
	          inner join results r on r.id = upt.result_id
	          where upt.status <> 'archived' 
	          order by upt.created_at desc
	      ", :page => params[:page], :per_page => 15) 
	    else
	          @print = UserPrepareTest.paginate_by_sql("        
	          select 
	          upt.id, u.avatar,  c.course_name, y.year_name, s.section_name, upt.user_id, u.first_name, u.last_name, u.email, u.last_sign_in_ip, upt.time_start, upt.time_end,
	          ( select created_at from user_tests where user_prepare_test_id = upt.id limit 1  )as created_at,
	          t.test_name, upt.status, r.title, upt.test_id
	          from user_prepare_tests upt
	          inner join users u on u.id = upt.user_id
	          inner join tests t on t.id = upt.test_id
	          inner join courses c on c.id = u.course_id
	          inner join years y on y.id = u.year_id
	          inner join sections s on s.id = u.section_id
	          inner join results r on r.id = upt.result_id
	          where upt.status <> 'archived' and lower(u.first_name) like '%#{params[:title].downcase}%'
	           or upt.status <> 'archived' and lower(u.last_name) like '%#{params[:title].downcase}%' or upt.status <> 'archived' and lower(c.course_name) like '%#{params[:title].downcase}%'
	           or upt.status <> 'archived' and lower(y.year_name) like '%#{params[:title].downcase}%' or upt.status <> 'archived' and lower(s.section_name) like '%#{params[:title].downcase}%'
	           or upt.status <> 'archived' and lower(t.test_name) like '%#{params[:title].downcase}%' or upt.status <> 'archived' and lower(r.title) like '%#{params[:title].downcase}%'
	           or upt.status <> 'archived' and lower(upt.user_id) like '%#{params[:title].downcase}%' 
	          order by upt.created_at desc
	      ", :page => params[:page], :per_page => 15) 
	    end
	end
	def print_report
        if session[:title].blank?
	        @print = UserPrepareTest.find_by_sql("        
	          select @s:=@s+1 serial_number,
	          upt.id, u.avatar, c.course_name, y.year_name, s.section_name,  upt.user_id, u.first_name, u.last_name, u.email, u.last_sign_in_ip, upt.time_start, upt.time_end,
	          ( select created_at from user_tests where user_prepare_test_id = upt.id limit 1  )as created_at,
	           t.test_name, upt.status, r.title, upt.test_id
	          from user_prepare_tests upt
	          inner join users u on u.id = upt.user_id
	          inner join tests t on t.id = upt.test_id
	          inner join courses c on c.id = u.course_id
	          inner join years y on y.id = u.year_id
	          inner join sections s on s.id = u.section_id
	          inner join results r on r.id = upt.result_id
	          , (SELECT @s:= 0) AS s
	          where upt.status <> 'archived'
	          order by serial_number asc, upt.created_at desc
	      ") 
	    else
	          @print = UserPrepareTest.find_by_sql("        
	          select @s:=@s+1 serial_number,
	          upt.id, u.avatar,  c.course_name, y.year_name, s.section_name, upt.user_id, u.first_name, u.last_name, u.email, u.last_sign_in_ip, upt.time_start, upt.time_end,
	          ( select created_at from user_tests where user_prepare_test_id = upt.id limit 1  )as created_at,
	          t.test_name, upt.status, r.title, upt.test_id
	          from user_prepare_tests upt
	          inner join users u on u.id = upt.user_id
	          inner join tests t on t.id = upt.test_id
	          inner join courses c on c.id = u.course_id
	          inner join years y on y.id = u.year_id
	          inner join sections s on s.id = u.section_id
	          inner join results r on r.id = upt.result_id
	          , (SELECT @s:= 0) AS s
	          where upt.status <> 'archived' and lower(u.first_name) like '%#{session[:title].downcase}%'
	           or lower(u.last_name) like '%#{session[:title].downcase}%' or lower(c.course_name) like '%#{session[:title].downcase}%'
	           or lower(y.year_name) like '%#{session[:title].downcase}%' or lower(s.section_name) like '%#{session[:title].downcase}%'
	           or lower(t.test_name) like '%#{session[:title].downcase}%' or lower(r.title) like '%#{session[:title].downcase}%'
	           or lower(upt.user_id) like '%#{session[:title].downcase}%' 
	          order by serial_number asc, upt.created_at desc
	      ") 
	    end

	    respond_to do |format|
	      format.html
	      format.pdf do
	        pdf = ReportPdf.new(@print)
	        send_data pdf.render, filename: 'report.pdf', type: 'application/pdf'
	      end
	    end
	end


		def print_single_report
        if session[:title].blank?
	        @print = UserPrepareTest.find_by_sql(["        
	          select @s:=@s+1 serial_number,
	          upt.id, u.avatar, c.course_name, y.year_name, s.section_name,  upt.user_id, u.first_name, u.last_name, u.email, u.last_sign_in_ip, upt.time_start, upt.time_end,
	          ( select created_at from user_tests where user_prepare_test_id = upt.id limit 1  )as created_at,
	           t.test_name, upt.status, r.title, upt.test_id
	          from user_prepare_tests upt
	          inner join users u on u.id = upt.user_id
	          inner join tests t on t.id = upt.test_id
	          inner join courses c on c.id = u.course_id
	          inner join years y on y.id = u.year_id
	          inner join sections s on s.id = u.section_id
	          inner join results r on r.id = upt.result_id
	          , (SELECT @s:= 0) AS s
	          where u.id = ? and upt.test_id = ? 
	          order by serial_number asc, upt.created_at desc
	      ", params[:user_id], params[:test_id]]) 
	    else
	          @print = UserPrepareTest.find_by_sql(["        
	          select @s:=@s+1 serial_number,
	          upt.id, u.avatar,  c.course_name, y.year_name, s.section_name, upt.user_id, u.first_name, u.last_name, u.email, u.last_sign_in_ip, upt.time_start, upt.time_end,
	          ( select created_at from user_tests where user_prepare_test_id = upt.id limit 1  )as created_at,
	          t.test_name, upt.status, r.title, upt.test_id
	          from user_prepare_tests upt
	          inner join users u on u.id = upt.user_id
	          inner join tests t on t.id = upt.test_id
	          inner join courses c on c.id = u.course_id
	          inner join years y on y.id = u.year_id
	          inner join sections s on s.id = u.section_id
	          inner join results r on r.id = upt.result_id
	          , (SELECT @s:= 0) AS s
	          where lower(u.first_name) like '%#{session[:title].downcase}%'
	           or lower(u.last_name) like '%#{session[:title].downcase}%' or lower(c.course_name) like '%#{session[:title].downcase}%'
	           or lower(y.year_name) like '%#{session[:title].downcase}%' or lower(s.section_name) like '%#{session[:title].downcase}%'
	           or lower(t.test_name) like '%#{session[:title].downcase}%' or lower(r.title) like '%#{session[:title].downcase}%'
	           or lower(upt.user_id) like '%#{session[:title].downcase}%'
	           and u.id = ? and upt.test_id = ? 
	          order by serial_number asc, upt.created_at desc
	      ", params[:user_id], params[:test_id]]) 
	    end

	    respond_to do |format|
	      format.html
	      format.pdf do
	        pdf = SingleReportPdf.new(@print)
	        send_data pdf.render, filename: 'user_report.pdf', type: 'application/pdf'
	      end
	    end
	end
end
