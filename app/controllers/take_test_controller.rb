class TakeTestController < ApplicationController
	 before_action :authenticate_user!
   before_action :user_prepare_test
   layout 'admin_lte_2'

  def take_test
    @user_prepare_test_check = UserPrepareTest.where(user_id: current_user.id, test_id: params[:id]).first
  	@take_test = TestQuestion.where(test_id: params[:id])
    @page_header = Test.find(params[:id]).test_name
    @page_desc = "View all questions"
 
  end

  def profile
        @page_header = "User Management"
    @page_desc = "Edit user account"
    @course = Course.where(status: 'Active')
    @year = Year.where(status: 'Active')
    @section = Section.where(status: 'Active')
    @user =  User.find(current_user.id)
    @test = Test.all

  end


  def update_profile
    @user =  User.find(params[:user_id])


    if @user.update(user_params)


    end



  render :layout => false
  end

  def submit_test
  	@take_test = TestQuestion.where(test_id: params[:test_id])
    upt_status = UserPrepareTest.where(user_id: current_user.id, test_id: params[:test_id]).first
    upt_status.update_attributes(status: "taken") 
	@take_test.each_with_index do |p, index| 
	
		countnum = index + 1     
		ut = UserTest.new
		ut.question_id = p.question_id
    ut.user_prepare_test_id = upt_status.id
		ut.question_answer_id = params[:"question#{countnum}"]
		ut.save
	end

    @get_result_id = UserTest.find_by_sql(["SELECT user_tests.question_answer_id, COUNT(*) AS ct,
      (select result_id from question_answers where id = user_tests.question_answer_id )as answer_result
FROM user_tests
Inner join user_prepare_tests on user_prepare_tests.id = user_tests.user_prepare_test_id
WHERE user_prepare_tests.user_id = ? and user_prepare_tests.test_id = ? and question_answer_id is not null
GROUP BY  answer_result
ORDER BY ct desc
",current_user.id, params[:test_id]] )

    upt_status.update_attributes(status: "taken", 
      result_id: @get_result_id.first.answer_result,
      time_start: params[:time_start],
      # time_end: params[:time_end].to_date.strftime('%H:%M:%S').split(':').map { |a| a.to_i }.inject(0) { |a, b| a * 60 + b}) 
      time_end: params[:time_start].to_time + (params[:time_end].split(':').map { |a| a.to_i }.inject(0) { |a, b| a * 60 + b}).to_i.seconds)
    
    # raise (params[:time_end].split(':').map { |a| a.to_i }.inject(0) { |a, b| a * 60 + b}).to_yaml
    # raise (Time.now.to_date.strftime('%d-%m-%Y %H:%M:%S') - Time.now.to_formatted_s(:time)).to_yaml
    # raise params[:time_start].to_date -  (params[:time_end].split(':').map { |a| a.to_i }.inject(0) { |a, b| a * 60 + b}).seconds)
    # raise Time.now.to_date.strftime('%d-%m-%Y' + " " + params[:time_end].to_s).to_yaml
  	# render :layout => false
  	redirect_to result_path(test_id:params[:test_id])
  end

  def result
   @page_header = "Test Result"
   @page_desc = "Display test result"



    # get_result_id = UserTest.select("MAX(question_answer_id) as max").where(user_id: current_user.id, test_id: params[:test_id]).first
    @get_result_id = UserTest.find_by_sql(["SELECT user_tests.question_answer_id, COUNT(*) AS ct,
      (select result_id from question_answers where id = user_tests.question_answer_id )as answer_result
FROM user_tests
Inner join user_prepare_tests on user_prepare_tests.id = user_tests.user_prepare_test_id
WHERE user_prepare_tests.user_id = ? and user_prepare_tests.test_id = ? and question_answer_id is not null
GROUP BY  answer_result
ORDER BY ct desc
",current_user.id, params[:test_id]] )
 @result = Result.find(@get_result_id.first.answer_result)
    # @results = Result.find(get_result_id.ct)
  end

private

  def user_params
    params.require(:user).permit(:email, :first_name, :last_name, :contact_no, :password, :password_confirmation, :avatar, :year_id, :course_id, :section_id)
  end




end
