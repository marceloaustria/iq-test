class UsersController < ApplicationController

 before_action :authenticate_user!
 before_action :authenticate_role
  layout 'admin_lte_2'


  def index
  	@page_header = "User Management"
  	@page_desc = "List user account"
    if params[:title].blank?
  	@user =  User.where(status: 'active').order("created_at desc").paginate(:page => params[:page], :per_page => 15)
  	else
      @user =  User.where(status: 'active').order("created_at desc").where("lower(first_name) like '%#{params[:title].downcase}%' or lower(last_name) like '%#{params[:title].downcase}%' or lower(email) like '%#{params[:title].downcase}%' or lower(id) like '%#{params[:title].downcase}%'" ).paginate(:page => params[:page], :per_page => 15)
     # render :layout => false
    end
  end



  def dashboard
    @page_header = "Dashboard"
    @page_desc = "Version 1"

    @user = User.where(status: 'active')
    @analytic = UserPrepareTest.where.not(status: 'archived')
    @question = Question.where(status: 'active')
    @test = Test.where(status: 'active')
    @us3r = User.where(role: "member", status: "active").order("created_at desc").limit(26)
    @analytics =  UserPrepareTest.find_by_sql("        
          select 
          upt.id, u.avatar, upt.user_id, u.first_name, u.last_name, u.email, u.last_sign_in_ip,
          ( select created_at from user_tests where user_prepare_test_id = upt.id limit 1  )as created_at,
          u.avatar, t.test_name, upt.status, r.title, upt.user_id, upt.test_id
          from user_prepare_tests upt
          inner join users u on u.id = upt.user_id
          inner join tests t on t.id = upt.test_id
          left join results r on r.id = upt.result_id
          where upt.status <> 'archived' and u.status = 'active'
          order by created_at desc
          limit 5
      ")
    

  end

  def setting
  	@page_header = "Settings"
  	@page_desc = "Manage accounts"


  end
  

  def edit
  	@page_header = "User Management"
  	@page_desc = "Edit user account"
    @role = ["Admin", "Member"]
  	@user =  User.find(params[:id])
    @course = Course.where(status: 'Active')
    @year = Year.where(status: 'Active')
    @section = Section.where(status: 'Active')
    @test = Test.where(status: 'active')
    utp = UserPrepareTest.where("user_id = #{@user.id} and status <> 'archived'")
    @user_test_prepare = []

    utp.each do |x| 
      @user_test_prepare << x.test_id 
    end

  end

  def delete
    @user_delete =  User.find(params[:id])
    @user_prepare_test =  UserPrepareTest.where(user_id: params[:id])

  @user_test =  UserTest.where(user_prepare_test_id: @user_prepare_test.map(&:id))
 
    # @user_prepare_test.destroy_all if @user_prepare_test.present?
     @user_prepare_test.update_all(status: "archived") if @user_prepare_test.present?
    # @user_test.destroy_all 
     @user_test.update_all(status: "archived") if @user_test.present?
       # @user_delete.destroy
       @user_delete.update_attributes(status: "archived")
    @user =  User.where(status: "active").paginate(:page => params[:page], :per_page => 15)
    render :layout => false
  end

  def update
  	@user =  User.find(params[:user_id])
  	if @user.update(user_params)
      if params[:user_prepare_test].present?    
          prepare_test = params[:user_prepare_test].reject { |q| q[:test_id].blank?}
          if prepare_test.present?
                 # UserPrepareTest.where(user_id: @user.id).destroy_all

                   prepare_test.each_with_index do |p, index|
                   get_upt = UserPrepareTest.where(user_id: @user.id, test_id: p[:test_id]).first 
                   if get_upt.blank?
                            qa = UserPrepareTest.new()
                            qa.user_id  = @user.id
                            qa.test_id = p[:test_id] 
                            qa.status = 'take'
                            qa.save
                   end
                      
                         
                  end
          end
      end
    end
  render :layout => false
  end

  def new
    @user = User.new
    @page_header = "User Management"
  	@page_desc = "Add user account"
    @test = Test.where(status: "active")
    @role = ["Member", "Admin"]
    @course = Course.where(status: 'Active')
    @year = Year.where(status: 'Active')
    @section = Section.where(status: 'Active')
  end

  def add_user
    @user = User.new(user_params)
     if @user.save
          if params[:user_prepare_test].present?  
            prepare_test = params[:user_prepare_test].reject { |q| q[:test_id].blank?}
            if prepare_test.present?
                prepare_test.each_with_index do |p, index|      
                  qa = UserPrepareTest.new()
                  qa.user_id  = @user.id
                  qa.test_id = p[:test_id] 
                  qa.status = 'take'
                  if qa.save
                  end
                end
            end
          end    
     end
     render :layout => false
  end

  private

	def user_params
	  params.require(:user).permit(:email, :first_name, :last_name, :password, :password_confirmation, :role, :contact_no, :avatar, :year_id, :course_id, :section_id)
	end

end
