class QuestionSectionsController < ApplicationController
	  before_action :authenticate_user!
    before_action :authenticate_role

  def index
  	@page_header = "Question Test Type"
  	@page_desc = "Create new test type"
    if params[:question_section_name].blank?
  	@question_section = QuestionSection.where(status: "active").order("id desc").paginate(:page => params[:page], :per_page => 15)
  else
        @question_section = QuestionSection.where(status: "active").order("id desc").where("lower(question_section_name) like '%#{params[:question_section_name].downcase}%'").paginate(:page => params[:page], :per_page => 15)
  end
  end

  def result_create
  	@question_section = QuestionSection.new(r_params)
  	if @question_section.save
  	@qu3stion_section = true
  	end
  	@question_section = QuestionSection.where(status: "active").order("id desc").paginate(:page => params[:page], :per_page => 15)
  	render :layout => false
  end

  def delete

  	@r =  QuestionSection.update(params[:id], status: "archived")
    # @upt = UserPrepareTest.where(test_id: params[:id])
    # @ut =  UserTest.where(user_prepare_test_id: @upt.map(&:id))
    # @qa =  QuestionAnswer.where(answer_result: params[:id])
    # @q = Question.where(id: @qa.map(&:question_id))
    # @upt.destroy_all if @upt.present?
    # @ut.destroy_all if @ut.present?
    # @qa.destroy_all if @qa.present?
    # @q.destroy_all if @q.present?


  	@question_section = QuestionSection.where(status: "active").order("id desc").paginate(:page => params[:page], :per_page => 15)
  	render :layout => false
  end

  def edit
    @page_header = "Question Test Type"
    @page_desc = "Edit Test type"
    @edit_question_section = QuestionSection.find(params[:id])
    @question_section = QuestionSection.where(status: "active").order("id desc").paginate(:page => params[:page], :per_page => 15)
  end

  def update
    if @qu3stion_section = QuestionSection.update(params[:question_section_id],r_params)
    end
    @question_section = QuestionSection.where(status: "active").order("id desc").paginate(:page => params[:page], :per_page => 15)
    render :layout => false
   
  end

private
  
  def r_params
    params.require(:question_sections).permit(:question_section_name)
  end

end
