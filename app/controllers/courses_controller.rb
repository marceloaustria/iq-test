class CoursesController < ApplicationController
	  before_action :authenticate_user!
    before_action :authenticate_role

  def index
  	@page_header = "Course Management"
  	@page_desc = "Create new Course"
    if params[:course_name].blank?
  	@course = Course.where(status: "active").order("id desc").paginate(:page => params[:page], :per_page => 15)
  else
        @course = Course.where(status: "active").order("id desc").where("lower(course_name) like '%#{params[:course_name].downcase}%'").paginate(:page => params[:page], :per_page => 15)
  end
  end

  def result_create
  	@course = Course.new(r_params)
  	if @course.save
  	@cours3 = true
  	end
  	@course = Course.where(status: "active").order("id desc").paginate(:page => params[:page], :per_page => 15)
  	render :layout => false
  end

  def delete
  	@r =  Course.update(params[:id], status: "archived")
  	@course = Course.where(status: "active").order("id desc").paginate(:page => params[:page], :per_page => 15)
  	render :layout => false
  end

  def edit
    @page_header = "Course Management"
    @page_desc = "Edit Course"
    @edit_course = Course.find(params[:id])
    @course = Course.where(status: "active").order("id desc").paginate(:page => params[:page], :per_page => 15)
  end

  def update
    if @cours3 = Course.update(params[:course_id],r_params)
    end
    @course = Course.where(status: "active").order("id desc").paginate(:page => params[:page], :per_page => 15)
    render :layout => false
   
  end

private
  
  def r_params
    params.require(:courses).permit(:course_name)
  end

end
