class ResultsController < ApplicationController
	  before_action :authenticate_user!
    before_action :authenticate_role

  def index
  	@page_header = "Result Management"
  	@page_desc = "Create new Results"
    if params[:title].blank?
  	@result = Result.where(status: "active").order("id desc").paginate(:page => params[:page], :per_page => 15)
  else
        @result = Result.where(status: "active").order("id desc").where("lower(title) like '%#{params[:title].downcase}%' or lower(description) like '%#{params[:title].downcase}%' ").paginate(:page => params[:page], :per_page => 15)
  end
  end

  def result_create
  	@result = Result.new(r_params)
  	if @result.save
  	@re3sult = true
  	end
  	@result = Result.where(status: "active").order("id desc").paginate(:page => params[:page], :per_page => 15)
  	render :layout => false
  end

  def delete

  	@r =  Result.update(params[:id], status: "archived")
    # @upt = UserPrepareTest.where(test_id: params[:id])
    # @ut =  UserTest.where(user_prepare_test_id: @upt.map(&:id))
    # @qa =  QuestionAnswer.where(answer_result: params[:id])
    # @q = Question.where(id: @qa.map(&:question_id))
    # @upt.destroy_all if @upt.present?
    # @ut.destroy_all if @ut.present?
    # @qa.destroy_all if @qa.present?
    # @q.destroy_all if @q.present?


  	@result = Result.where(status: "active").order("id desc").paginate(:page => params[:page], :per_page => 15)
  	render :layout => false
  end

  def edit
    @page_header = "Result Management"
    @page_desc = "Edit Results"
    @edit_result = Result.find(params[:id])
    @result = Result.where(status: "active").order("id desc").paginate(:page => params[:page], :per_page => 15)
  end

  def update
    if @r3sult = Result.update(params[:result_id],r_params)
    end
    @result = Result.where(status: "active").order("id desc").paginate(:page => params[:page], :per_page => 15)
    render :layout => false
   
  end

private
  
  def r_params
    params.require(:results).permit(:title, :description)
  end

end
