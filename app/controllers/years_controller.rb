class YearsController < ApplicationController
	  before_action :authenticate_user!
    before_action :authenticate_role

  def index
  	@page_header = "Year Management"
  	@page_desc = "Create new year"
    if params[:year_name].blank?
  	@year = Year.where(status: "active").order("id desc").paginate(:page => params[:page], :per_page => 15)
  else
        @year = Year.where(status: "active").order("id desc").where("lower(year_name) like '%#{params[:year_name].downcase}%'").paginate(:page => params[:page], :per_page => 15)
  end
  end

  def result_create
  	@year = Year.new(r_params)
  	if @year.save
  	@y3ar = true
  	end
  	@year = Year.where(status: "active").order("id desc").paginate(:page => params[:page], :per_page => 15)
  	render :layout => false
  end

  def delete
  	@r =  Year.update(params[:id], status: "archived")
  	@year = Year.where(status: "active").order("id desc").paginate(:page => params[:page], :per_page => 15)
  	render :layout => false
  end

  def edit
    @page_header = "Year Management"
    @page_desc = "Edit Year"
    @edit_year = Year.find(params[:id])
    @year = Year.where(status: "active").order("id desc").paginate(:page => params[:page], :per_page => 15)
  end

  def update
    if @y3ar = Year.update(params[:year_id],r_params)
    end
    @year = Year.where(status: "active").order("id desc").paginate(:page => params[:page], :per_page => 15)
    render :layout => false
   
  end

private
  
  def r_params
    params.require(:years).permit(:year_name)
  end

end
