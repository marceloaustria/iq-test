class HomeController < ApplicationController
  layout 'home'

  def index
  end

  def instructions
  end

  def about 
  end

  def about
  end

  def terms_conditions
  end
end
