class TestsController < ApplicationController
  before_action :authenticate_user!
  before_action :authenticate_role
	
  layout 'admin_lte_2'

  def index
  	@page_header = "Test Management"
  	@page_desc = "You can add, edit and archived"
    if params[:title].blank?
  	   @test =  Test.where(status: "active").paginate(:page => params[:page], :per_page => 15)
    else
        @test =  Test.where(status: "active").where("lower(test_name) like '%#{params[:title].downcase}%' ").paginate(:page => params[:page], :per_page => 15)

      end
  end

  def delete_test
    # test_questions test_id
    # tests id
    # user_prepare_tests test_id
    # user_tests user_prepare_test_id



    # @upt = UserPrepareTest.where(test_id: params[:id])
    # @ut =  UserTest.where(user_prepare_test_id: @upt.map(&:id))
    # @upt.destroy_all if @upt.present?
    # @ut.destroy_all if @ut.present?
    # @tq = TestQuestion.where(test_id: params[:id]).destroy_all
    # @t = Test.where(id: params[:id]).destroy_all
    
    @t =  Test.update(params[:id], status: "archived")
    @test =  Test.where(status: "active").paginate(:page => params[:page], :per_page => 15)
    render :layout => false
  end

  def new
  	t = Test.find(params[:id])
  	@page_header = t.test_name
  	@page_desc = "Manage Answering Question"
  	@q_section = Test.get_question_section_active(params[:id])
  	@test_question =  Test.get_test_question(params[:id])

  end

  def create
  	@test = Test.new(t_params)
  		#save the test
  		if @test.save 	
  			redirect_to new_test_path(id:@test.id)
  		else
  		end
  end



  def delete
    @test = TestQuestion.where(test_id: params[:id]).destroy_all
    @q_section = Test.get_question_section_active(params[:id])
    @test_question =  Test.get_test_question(params[:id])

    render :layout => false
  end

  def preview_test

    t = Test.find(params[:id])
    @page_header = t.test_name
    @page_desc = "Preview Test"

    @take_test = TestQuestion.where(test_id: params[:id])
      # render layout: 'test'
  end

  def create_test_question
  	get_test = Question.where(question_section_id: params[:question][:question_section_id], status: 'active')
  	 get_test.each_with_index do |p, index|  
  	 		tq = TestQuestion.new
  	 		tq.question_id = p.id
  	 		tq.test_id = params[:test_id]
  	 		tq.question_section_id = p.question_section_id
  	 		tq.save
  	 		@result = true
  	 end  

  	 @test_question =  Test.get_test_question(params[:test_id])
  	 @q_section = Test.get_question_section_active(params[:test_id])
  	 render :layout => false
  end


  def update_status

      @test =  Test.find(params[:id])
      @test.update_attributes(display_public: params[:display_public])
       render :layout => false
  end

  private

  def t_params
    	params.require(:tests).permit(:test_name, :status, :display_public  )
  end


end
