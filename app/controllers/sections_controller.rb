class SectionsController < ApplicationController
	  before_action :authenticate_user!
    before_action :authenticate_role

  def index
  	@page_header = "Section Management"
  	@page_desc = "Create new section"
    if params[:section_name].blank?
  	@section = Section.where(status: "active").order("id desc").paginate(:page => params[:page], :per_page => 15)
  else
        @section = Section.where(status: "active").order("id desc").where("lower(section_name) like '%#{params[:section_name].downcase}%'").paginate(:page => params[:page], :per_page => 15)
  end
  end

  def result_create
  	@section = Section.new(r_params)
  	if @section.save
  	@s3ction = true
  	end
  	@section = Section.where(status: "active").order("id desc").paginate(:page => params[:page], :per_page => 15)
  	render :layout => false
  end

  def delete
  	@r =  Section.update(params[:id], status: "archived")
  	@section = Section.where(status: "active").order("id desc").paginate(:page => params[:page], :per_page => 15)
  	render :layout => false
  end

  def edit
    @page_header = "Section Management"
    @page_desc = "Edit Section"
    @edit_section = Section.find(params[:id])
    @section = Section.where(status: "active").order("id desc").paginate(:page => params[:page], :per_page => 15)
  end

  def update
    if @s3ction = Section.update(params[:section_id],r_params)
    end
    @section = Section.where(status: "active").order("id desc").paginate(:page => params[:page], :per_page => 15)
    render :layout => false
   
  end

private
  
  def r_params
    params.require(:sections).permit(:section_name)
  end

end
