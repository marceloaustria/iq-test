class ApplicationController < ActionController::Base
   layout :layout_by_resource
   before_action :configure_permitted_parameters, if: :devise_controller? 
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  # protect_from_forgery with: :null_session



  def layout_by_resource
  	# and user_signed_in?
   if devise_controller? 
      'admin_lte_2_login'
    else
      'admin_lte_2'
    end
  end




  def authenticate_role
    if current_user.present?
      if current_user.role == 'admin'
        #super admin...
      elsif current_user.role == 'member' 
          redirect_to new_user_session_path
      end
    end  
  end



  private

  def configure_permitted_parameters
		devise_parameter_sanitizer.permit(:sign_up) do |u|
        u.permit(:first_name, :last_name, :about,  :email, :password, :password_confirmation, :role)
        end

        devise_parameter_sanitizer.permit(:account_update) do |u|
          u.permit(:first_name, :last_name, :about,  :email, :password, :password_confirmation, :role)
        end      
  end

  def after_sign_in_path_for(resource)
     if user_signed_in?
         if current_user.present?
          if current_user.role == 'admin'
             dashboard_path
          elsif current_user.role == 'member'
             profile_path
          end   
        else
          profile_path
        end

    end
  end

  def user_prepare_test
    if current_user.present?
      if current_user.role == 'admin'
        #super admin...
      elsif current_user.role == 'member' 
          @user_prepare_test = UserPrepareTest.where(user_id: current_user.id)
      end
    end
  end


  # case resource
  # when User then start_test_path
  # when Admin then  questions_path
  # end

  

end
