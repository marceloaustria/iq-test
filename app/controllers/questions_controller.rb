class QuestionsController < ApplicationController
  before_action :authenticate_user!
  before_action :authenticate_role

  def index

    @page_header = "All Questions"
    @page_desc = "View all questions"

    # @question = Question.get_question
    # select q.id, q.question_name, q.created_at, qs.question_section_name from questions q
    #       INNER JOIN question_sections as qs on qs.id = q.question_section_id
    
    if params[:title].blank?
     @question = Question.joins(:question_section).select("questions.id, questions.question_name, questions.created_at, question_sections.question_section_name").where("questions.status = 'active'").order("questions.id desc").paginate(:page => params[:page], :per_page => 15)
    else
     @question = Question.joins(:question_section).select("questions.id, questions.question_name, questions.created_at, question_sections.question_section_name").order("questions.id desc").where("questions.status = 'active' and lower(questions.question_name) like '%#{params[:title].downcase}%' " ).paginate(:page => params[:page], :per_page => 15)
     # render :layout => false
    end

    # .where("bars.foo_id != ? and users.created_at <= ?", 5, 50.days.ago).select("distinct users.*") 
  end


  def multiple_choice

    @page_header = "Question Management"
    @page_desc = "Create new questions"
  	@q_section = QuestionSection.where(status: "active")
    @result = Result.where(status: "active")

  end

  def analytics
    @page_header = "Analytics"
    @page_desc = "Results Analytics"
  end

  # def test_library
  #   @page_header = "Test Library"
  #   @page_desc = "View questions library"
  # end


  def multiple_choice_create
  		@question = Question.new(q_params)
  		# @question.question_type_id = 1
  		#save the question
      q_ans = params[:question_ans].reject { |q| q[:answer].blank? || q[:result].blank?}
  		if @question.save && q_ans.present?
  			# question_ans[q_answer]
  			#save the question answer
	  	        # q_ans = params[:question_ans].reject { |q| q[:answer].blank? || q[:result].blank? }
              # q_result = params[:question_ans].reject { |q| q[:result].blank? }
	  	        q_ans.each_with_index do |p, index|      
                qa = QuestionAnswer.new()
	      				qa.question_answer_name = p[:answer] 
                qa.result_id = p[:result] 
	      				qa.question_id = @question.id
	      				if qa.save
	      				end
	  	        end

          check_exist = TestQuestion.where(question_section_id: @question.question_section_id).first
          if check_exist.present?
            tq = TestQuestion.new()
            tq.question_id = @question.id
            tq.test_id = check_exist.test_id 
            tq.question_section_id = @question.question_section_id
            if tq.save
            end
          end
        @result = true          
  		else

  		end
  		 # render :inline => "success"
       render :layout => false
  		 # debug :params
  end



  def update_multiple_choice
    if Question.update(params[:question_id],q_params)
      q_ans = params[:question_ans].reject { |q| q[:answer].blank? || q[:result].blank?}
      if q_ans.present?
            QuestionAnswer.where(question_id: params[:question_id]).destroy_all
              q_ans.each_with_index do |p, index|      
                qa = QuestionAnswer.new()
                qa.question_answer_name = p[:answer] 
                qa.result_id = p[:result] 
                qa.question_id = params[:question_id]
                if qa.save
                end
              end

        @result = true          
      else

      end
    end
    render :layout => false
    # render :inline => "success"

  end

  def edit_multiple_choice
    @page_header = "Question Management"
    @page_desc = "Edit questions"
    @question = Question.find(params[:id])
    @question_ans = QuestionAnswer.where(question_id: params[:id])
    @q_section = QuestionSection.all
    @result = Result.where(status: "active")
  end

  def delete_multiple_choice  
    # @q = Question.where(id: params[:id]).destroy_all
    @q =  Question.update(params[:id], status: "archived")
    @tq = QuestionAnswer.where(question_id: params[:id]).destroy_all
    @question = Question.joins(:question_section).select("questions.id, questions.question_name, questions.created_at, question_sections.question_section_name").where("questions.status = 'active'").order("questions.id desc").paginate(:page => params[:page], :per_page => 15)
    render :layout => false
  end

 
  def question_section_create
  	@q_section = QuestionSection.new(q_section_params)
  		#save the question
  		if @q_section.save 	
  			respond_to do |format|
			format.js
			format.html { render :partial => QuestionSection.order("created_at desc") }
			end

  		else
  			@result = false
  		end
  			@q_section = QuestionSection.all
  end




private



  def q_params
    	params.require(:question).permit(:question_name, :question_point, :question_section_id)
  end

  

  def q_section_params
    	params.require(:question_section).permit(:question_section_name)
  end

  
end
