module ApplicationHelper


	def is_active(controller, cont, action, act)
		if act == action and cont == controller
			return "active"
		end		
	end

	def role_label(role)
		if role == "admin"
			return	"<span class='label label-primary'>#{role.titleize}</span>".html_safe
		else
			return "<span class='label label-success'>#{role.titleize}</span>".html_safe	
		end
	end

	def status_label(status)
		if status == "taken"
			return	"<span class='label label-success'>#{status.titleize}</span>".html_safe
		elsif status == "retake"	
			return	"<span class='label label-warning'>#{status.titleize}</span>".html_safe
		else
			return "<span class='label label-info'>#{status.titleize}</span>".html_safe	
		end
	end

	def avatar_photo(id, photo_filename)
	 return	"/uploads/user/avatar/#{id}/thumb_#{photo_filename}"
	end

	def time_diff(start_time, end_time)
	  seconds_diff = (start_time - end_time).to_i.abs

	  hours = seconds_diff / 3600
	  seconds_diff -= hours * 3600

	  minutes = seconds_diff / 60
	  seconds_diff -= minutes * 60

	  seconds = seconds_diff

	  "#{hours.to_s.rjust(2, '0')}:#{minutes.to_s.rjust(2, '0')}:#{seconds.to_s.rjust(2, '0')}"
	end

	def color(num)
		case num
		when 0
			# blue
		  return '#2196f3'
		when 1
			# light blue
		  return '#01579b'
		when 2
			# cyan
		  return '#006064'
		when 3
			# teal
		  return '#009688'
		when 4
			# green
		  return '#1b5e20'
		when 5
			# light green
		  return '#33691e'
		when 6
			# red
		  return '#f44336'
		when 7
		  	# orange
		  return '#e65100'
		when 8
		  	# deep orange
		  return '#bf360c'
		when 9
			# purple
		  return '#9c27b1'
		when 10
			# deep purple
		  return '#673ab7'
		when 11
			# indo
		  return '#3f51b5'
		when 12
			# pink
		  return '#e91e63'
		when 13
			# teal
		  return '#00796b'
		when 14
			# brown
		  return '#795548'
		when 15
			# blue grey
		  return '#607d8B'
		else
		  return '#263238'
		end

	end
end
