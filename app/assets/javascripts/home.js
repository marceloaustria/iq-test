
$(document).on('turbolinks:load', function () {
// $(turboforms);

    $(".q_section-select").select2({
     placeholder: "Select Type",
     allowClear: true
  });

        $(".q_result-select").select2({
     placeholder: "Select Result",
     allowClear: true
  });


    $(".q_year-select").select2({
     placeholder: "Select Year",
     allowClear: true
  });

        $(".q_course-select").select2({
     placeholder: "Select Course",
     allowClear: true
  });


    $('.q_section_modal').on('click', function(e){
        // $('#message_body').val("Hi, I'm interested in your posted ad " + $(this).data('title') + ".");
        $('#q_section_modal').modal({ backdrop: 'static', keyboard: false })
    });


    $('.t_create_modal').on('click', function(e){
        // $('#message_body').val("Hi, I'm interested in your posted ad " + $(this).data('title') + ".");
        $('#t_create_modal').modal({ backdrop: 'static', keyboard: false })
    });

Date.prototype.addSeconds= function(h){
this.setSeconds(this.getSeconds()+h);
return this;
}
var date = new Date().addSeconds(5) / 1000
$('.timer').countid({
  clock: true,
  dateTime: date,
  dateTplRemaining: "%S seconds to start timer",
  // dateTplElapsed: "%S seconds elapsed",
  complete: function( el ){
    el.css({ 'color': '#3c763d'})
  }
})
$('.timer2').countid({
  clock: true,
  dateTime: date,
  dateTplRemaining: "%H:%M:%S",
  dateTplElapsed: "%H:%M:%S",
  complete: function( el ){
    el.animate({ 'font-size': '25px'})
  }
})



//Override the default confirm dialog by rails
$.rails.allowAction = function(link){
  if (link.data("confirm") == undefined){
    return true;
  }
  $.rails.showConfirmationDialog(link);
  return false;
}

//User click confirm button
$.rails.confirmed = function(link){
  link.data("confirm", null);
  link.trigger("click.rails");
  return false;
}



//Display the confirmation dialog
$.rails.showConfirmationDialog = function(link){
  var message = link.data("confirm");

  var caption = link.data("caption");
  if (!$.trim(caption)){   
   caption = 'Yes, delete it!'
  }


  
  swal({
    title: message,
    text: "You won't be able to revert this!",
    type: 'warning',
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    showCancelButton: true,
    confirmButtonText: caption
  }).then(function(e){
    $.rails.confirmed(link);
  });
};


});    






// function checking_test(user_id) {
//   alert("s");
//     var request = $.ajax({
//         url: "/get-userdata?user_id=" + user_id,
//         type: "GET",            
//         dataType: "html"
//     });

//     request.done(function(msg) {
//         $(".user-number").html(msg);
//         $(".number-hide").hide();          
//     });

//     request.fail(function(jqXHR, textStatus) {
//         alert( "Request failed: " + textStatus );
//     });
// }
// var ready = function () {
//    alert('success');
//   var o;
//   o = $.AdminLTE.options;
//   if (o.sidebarPushMenu) {
//     $.AdminLTE.pushMenu.activate(o.sidebarToggleSelector);
//   }
//   return $.AdminLTE.layout.activate();
 
// };
// document.addEventListener('turbolinks:load', ready);
