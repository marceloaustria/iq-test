CKEDITOR.editorConfig = function (config) {
  // ... other configuration ...

  config.toolbar = [
  	{name: 'document', items: ['Source']},
  	{name: 'insert', items: ['Image']},
  	{name: 'clipboard', items: ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord']},
  	{name: 'editing', items: ['Scayt']},
  	{name: 'tools', items: ['Maximize']}
  
  ];
// config.toolbar = 'Basic';


    config.filebrowserBrowseUrl = "/ckeditor/attachment_files";
    config.filebrowserImageBrowseLinkUrl = "/ckeditor/pictures";
    config.filebrowserImageBrowseUrl = "/ckeditor/pictures";
    config.filebrowserImageUploadUrl = "/ckeditor/pictures";
    config.filebrowserUploadUrl = "/ckeditor/attachment_files"; 

// config.filebrowserImageBrowseUrl = '/ckeditor/pictures';	
 config.height = 100;        // 500 pixels high.




  // ... rest of the original config.js  ...
}

// http://localhost:3000/ckeditor/pictures?CKEditor=question_question_name&CKEditorFuncNum=1&langCode=en



 // CKEDITOR.replace( 'question_question_name', {
 //        height: 200
 //    } );
 
 