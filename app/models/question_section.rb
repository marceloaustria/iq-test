class QuestionSection < ActiveRecord::Base
	has_many :questions

  validates_presence_of :question_section_name
end
