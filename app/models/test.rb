class Test < ActiveRecord::Base

		def self.get_question_section_active(test_id)
		  	Test.find_by_sql([" 				
			SELECT 
				 qs.id, qs.question_section_name
				FROM question_sections qs
				Inner JOIN questions as q on q.question_section_id = qs.id

				LEFT JOIN  test_questions as tq on tq.question_section_id = qs.id
				LEFT JOIN tests as t on t.id = tq.test_id
				where qs.id not in (select distinct(question_section_id) from test_questions where test_id = ?)
				group by qs.id
			",test_id]) 
  		end

  		def self.get_test_question(test_id)
		  	Test.find_by_sql([" 				
				SELECT 
				 qs.id, qs.question_section_name, tq.created_at, tq.test_id
				FROM question_sections qs
				Inner JOIN questions as q on q.question_section_id = qs.id
				Inner JOIN test_questions as tq on tq.question_id = q.id
				WHERE tq.test_id = ?		
				GROUP BY  qs.question_section_name, qs.id

			", test_id]) 
  		end
end
