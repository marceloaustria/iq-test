class Question < ActiveRecord::Base
	has_many :question_answers
	belongs_to :question_section
	validates :question_name, :question_section_id, presence: true


	 # scope :get_question, lambda {||
	 # 		find_by_sql([" 
		# 				select q.id, q.question_name, q.created_at, qs.question_section_name from questions q
		# 			INNER JOIN question_sections as qs on qs.id = q.question_section_id
		# "])
	 # }



  		def self.get_question()
		  	Question.find_by_sql([" 				
					select q.id, q.question_name, q.created_at, qs.question_section_name from questions q
					INNER JOIN question_sections as qs on qs.id = q.question_section_id
			"]) 
  		end

end

