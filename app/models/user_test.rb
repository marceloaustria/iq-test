class UserTest < ActiveRecord::Base
	  		def self.get_report(test_id, user_id)
		  	UserTest.find_by_sql([" 				
						select q.question_name, qa.question_answer_name 
						from user_tests ut
						inner join questions q on q.id = ut.question_id
						left join question_answers qa on qa.id = ut.question_answer_id
						inner join user_prepare_tests upt on upt.id = ut.user_prepare_test_id
						where upt.test_id = ? and upt.user_id = ?
			",test_id, user_id]) 
  		end
end
