# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#




#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

    admin       = User.new(role: 'admin', email: 'admin@test.com', first_name: 'Maricel', last_name: 'Austria', password: 'P@$$w0rd', password_confirmation: "P@$$w0rd", created_at: DateTime.now, updated_at: DateTime.now)
    admin.save!
    puts "Admin account has been created"

    member      = User.new(role: 'member', email: 'member@test.com', first_name: 'Diana', last_name: 'Prince', password: 'P@$$w0rd', password_confirmation: "P@$$w0rd", created_at: DateTime.now, updated_at: DateTime.now)
    member.save!
	puts "Member account has been created"
