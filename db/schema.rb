# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180110080915) do

  create_table "ckeditor_assets", force: :cascade do |t|
    t.string   "data_file_name",    limit: 255, null: false
    t.string   "data_content_type", limit: 255
    t.integer  "data_file_size",    limit: 4
    t.string   "type",              limit: 30
    t.integer  "width",             limit: 4
    t.integer  "height",            limit: 4
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
  end

  add_index "ckeditor_assets", ["type"], name: "index_ckeditor_assets_on_type", using: :btree

  create_table "courses", force: :cascade do |t|
    t.string   "course_name", limit: 1000
    t.datetime "created_at",                                  null: false
    t.datetime "updated_at",                                  null: false
    t.string   "status",      limit: 255,  default: "active"
  end

  create_table "question_answers", force: :cascade do |t|
    t.integer  "question_id",          limit: 4
    t.string   "question_answer_name", limit: 255
    t.integer  "result_id",            limit: 4,   null: false
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
  end

  add_index "question_answers", ["result_id"], name: "fk_rails_05381cccca", using: :btree

  create_table "question_sections", force: :cascade do |t|
    t.string   "question_section_name", limit: 255
    t.datetime "created_at",                                           null: false
    t.datetime "updated_at",                                           null: false
    t.string   "status",                limit: 255, default: "active"
  end

  create_table "questions", force: :cascade do |t|
    t.string   "question_name",       limit: 1000
    t.integer  "question_section_id", limit: 4,                       null: false
    t.datetime "created_at",                                          null: false
    t.datetime "updated_at",                                          null: false
    t.string   "status",              limit: 255,  default: "active"
  end

  add_index "questions", ["question_section_id"], name: "fk_rails_7935e74bf0", using: :btree

  create_table "results", force: :cascade do |t|
    t.string   "title",       limit: 255
    t.text     "description", limit: 65535
    t.datetime "created_at",                                   null: false
    t.datetime "updated_at",                                   null: false
    t.string   "status",      limit: 255,   default: "active"
  end

  create_table "sections", force: :cascade do |t|
    t.string   "section_name", limit: 1000
    t.string   "status",       limit: 255,  default: "active"
    t.datetime "created_at",                                   null: false
    t.datetime "updated_at",                                   null: false
  end

  create_table "test_questions", force: :cascade do |t|
    t.integer  "question_id",         limit: 4, null: false
    t.integer  "test_id",             limit: 4, null: false
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
    t.integer  "question_section_id", limit: 4
  end

  add_index "test_questions", ["test_id"], name: "fk_rails_13e2124a19", using: :btree

  create_table "tests", force: :cascade do |t|
    t.string   "test_name",      limit: 255
    t.datetime "created_at",                                    null: false
    t.datetime "updated_at",                                    null: false
    t.string   "display_public", limit: 255, default: "active"
    t.string   "status",         limit: 255, default: "active"
  end

  create_table "user_prepare_tests", force: :cascade do |t|
    t.integer  "user_id",    limit: 4,   null: false
    t.integer  "test_id",    limit: 4,   null: false
    t.string   "status",     limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.integer  "result_id",  limit: 4
    t.datetime "time_start"
    t.datetime "time_end"
  end

  add_index "user_prepare_tests", ["test_id"], name: "fk_rails_722df65791", using: :btree
  add_index "user_prepare_tests", ["user_id"], name: "fk_rails_0296df7972", using: :btree

  create_table "user_tests", force: :cascade do |t|
    t.datetime "created_at",                                          null: false
    t.datetime "updated_at",                                          null: false
    t.integer  "question_id",          limit: 4,                      null: false
    t.integer  "question_answer_id",   limit: 4,                      null: false
    t.integer  "user_prepare_test_id", limit: 4,                      null: false
    t.string   "status",               limit: 255, default: "active"
  end

  add_index "user_tests", ["question_answer_id"], name: "fk_rails_23759337c9", using: :btree
  add_index "user_tests", ["question_id"], name: "fk_rails_c0b95b06dd", using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "email",                  limit: 255, default: "",       null: false
    t.string   "encrypted_password",     limit: 255, default: "",       null: false
    t.string   "reset_password_token",   limit: 255
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          limit: 4,   default: 0,        null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip",     limit: 255
    t.string   "last_sign_in_ip",        limit: 255
    t.datetime "created_at",                                            null: false
    t.datetime "updated_at",                                            null: false
    t.string   "first_name",             limit: 255
    t.string   "last_name",              limit: 255
    t.string   "avatar",                 limit: 255
    t.string   "role",                   limit: 255
    t.string   "contact_no",             limit: 255
    t.integer  "course_id",              limit: 4
    t.integer  "year_id",                limit: 4
    t.integer  "section_id",             limit: 4
    t.string   "status",                 limit: 255, default: "active"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  create_table "years", force: :cascade do |t|
    t.string   "year_name",  limit: 1000
    t.datetime "created_at",                                 null: false
    t.datetime "updated_at",                                 null: false
    t.string   "status",     limit: 255,  default: "active"
  end

  add_foreign_key "question_answers", "results"
  add_foreign_key "questions", "question_sections"
  add_foreign_key "test_questions", "tests"
  add_foreign_key "user_prepare_tests", "tests"
  add_foreign_key "user_prepare_tests", "users"
  add_foreign_key "user_tests", "question_answers"
  add_foreign_key "user_tests", "questions"
end
