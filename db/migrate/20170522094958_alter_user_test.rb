class AlterUserTest < ActiveRecord::Migration
	def self.up
		add_column :user_tests, :question_id, :integer
		add_column :user_tests, :test_id, :integer
		add_column :user_tests, :question_answer_id, :integer
	end
	def self.down
		remove_column :user_tests, :question_id
		remove_column :user_tests, :test_id
		remove_column :user_tests, :question_answer_id
	end
end
