class AddDisplayPublic < ActiveRecord::Migration
	def self.up
		add_column :tests, :display_public, :string, :default => "active"
		remove_column :tests, :status
	end
	def self.down
		remove_column :tests, :display_public
	end
end
