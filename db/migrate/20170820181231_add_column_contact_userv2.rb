class AddColumnContactUserv2 < ActiveRecord::Migration

  	def self.up
		change_column :users, :contact_no, :string
	end
	def self.down
		change_column :users, :contact_no, :integer
	end
end
