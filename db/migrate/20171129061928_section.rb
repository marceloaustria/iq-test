class Section < ActiveRecord::Migration
  def change
    create_table :sections do |t|
      t.string :section_name , limit: 1000
      t.string :status, :default => "active"
      t.timestamps null: false
    end
  end
end
