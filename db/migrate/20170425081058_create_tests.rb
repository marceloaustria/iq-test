class CreateTests < ActiveRecord::Migration
  def change
    create_table :tests do |t|
      t.integer :question_id
      t.string :test_name
      t.string :status

      t.timestamps null: false
    end
  end
end
