class AddUserTest < ActiveRecord::Migration
	def self.up
		add_column :user_tests, :user_prepare_test_id, :string
	end
	def self.down
		remove_column :user_tests, :user_prepare_test_id
	end
end
