class AddForeignKey < ActiveRecord::Migration
  def change
  	add_foreign_key :questions, :question_sections, foreign_key: true 
	# add_foreign_key :test_questions, :questions,  column: :question_id  
	add_foreign_key :test_questions, :tests, foreign_key: true  
	add_foreign_key :user_tests, :questions, foreign_key: true 
	add_foreign_key :user_tests, :question_answers, foreign_key: true 
	# add_foreign_key :user_tests, :user_prepare_tests, foreign_key: true 
	add_foreign_key :user_prepare_tests, :users, foreign_key: true 
	add_foreign_key :user_prepare_tests, :tests, foreign_key: true 
	add_foreign_key :question_answers, :results, foreign_key: true 
  end
end
