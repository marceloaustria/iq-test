class CreateTestQuestions < ActiveRecord::Migration
  def change
    create_table :test_questions do |t|
      t.integer :question_id
      t.integer :test_id

      t.timestamps null: false
    end
  end
end
