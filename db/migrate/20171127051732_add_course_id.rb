class AddCourseId < ActiveRecord::Migration
	def self.up
		add_column :users, :course_id, :integer
		add_column :users, :year_id, :integer
		remove_column :users, :course
		remove_column :users, :year
	end
	def self.down
		remove_column :users, :course_id
		remove_column :users, :year_id
		add_column :users, :course, :string
		add_column :users, :year, :string
	end

end
