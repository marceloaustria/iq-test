class AddColumnContactUser < ActiveRecord::Migration
	def self.up
		add_column :users, :contact_no, :integer
	end
	def self.down
		remove_column :users, :contact_no
	end
end
