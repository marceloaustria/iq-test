class CreateQuestionSections < ActiveRecord::Migration
  def change
    create_table :question_sections do |t|
      t.string :question_section_name

      t.timestamps null: false
    end
  end
end
