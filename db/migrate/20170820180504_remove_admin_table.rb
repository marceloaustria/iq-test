class RemoveAdminTable < ActiveRecord::Migration
  def self.up
	drop_table :admins
  end

  def self.down
    drop_table :admins
  end
end
