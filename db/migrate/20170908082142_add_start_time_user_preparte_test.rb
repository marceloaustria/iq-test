class AddStartTimeUserPreparteTest < ActiveRecord::Migration
	def self.up
		add_column :user_prepare_tests, :time_start, :datetime
		add_column :user_prepare_tests, :time_end, :datetime
	end
	def self.down
		remove_column :user_prepare_tests, :time_start
		remove_column :user_prepare_tests, :time_end
	end
end
