class AddStatusResult < ActiveRecord::Migration
	def self.up
		add_column :results, :status, :string, :default => "active"

	end
	def self.down
		remove_column :results, :status
	end

end
