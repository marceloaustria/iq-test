class CreateUserTests < ActiveRecord::Migration
  def change
    create_table :user_tests do |t|
      t.integer :user_id
      t.integer :result_id

      t.timestamps null: false
    end
  end
end
