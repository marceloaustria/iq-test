class QuestionDefaultStatus < ActiveRecord::Migration
	def self.up
		remove_column :questions, :status
		add_column :questions, :status, :string, :default => "active"
	end
	def self.down
		remove_column :questions, :status
	end
end
