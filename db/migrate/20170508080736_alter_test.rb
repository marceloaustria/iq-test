class AlterTest < ActiveRecord::Migration
	def self.up
		remove_column :tests, :question_id
	end
	def self.down
		add_column :tests, :question_id, :integer
	end
end
