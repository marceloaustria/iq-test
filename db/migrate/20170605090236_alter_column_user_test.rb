class AlterColumnUserTest < ActiveRecord::Migration
	def self.up
		remove_column :user_tests, :user_id
		remove_column :user_tests, :result_id
		remove_column :user_tests, :test_id
	end
	def self.down
		add_column :user_tests, :user_id, :integer
		add_column :user_tests, :result_id, :integer
		add_column :user_tests, :test_id, :integer
	end
end
