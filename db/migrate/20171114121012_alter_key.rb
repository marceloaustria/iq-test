class AlterKey < ActiveRecord::Migration
	def self.up
		rename_column :question_answers, :answer_result, :result_id
		remove_column :questions, :question_type_id
		remove_column :questions, :question_points
		change_column :questions, :question_section_id, :integer, null: false
		change_column :test_questions, :question_id, :integer, null: false
		change_column :test_questions, :test_id, :integer, null: false
		change_column :user_tests, :question_id, :integer, null: false
		change_column :user_tests, :question_answer_id, :integer, null: false
		change_column :user_tests, :user_prepare_test_id, :integer, null: false
		change_column :user_prepare_tests, :user_id, :integer, null: false
		change_column :user_prepare_tests, :test_id, :integer, null: false
		change_column :question_answers, :result_id, :integer, null: false
	end
	def self.down
		rename_column :question_answers, :result_id, :answer_result
		add_column :questions, :question_type_id, :integer
		add_column :questions, :question_points, :integer
		change_column :questions, :question_section_id, :integer, null: true
		change_column :test_questions, :question_id, :integer, null: true
		change_column :test_questions, :test_id, :integer, null: true
		change_column :user_tests, :question_id, :integer, null: true
		change_column :user_tests, :question_answer_id, :integer, null: true
		change_column :user_tests, :user_prepare_test_id, :integer, null: true
		change_column :user_prepare_tests, :user_id, :integer, null: true
		change_column :user_prepare_tests, :test_id, :integer, null: true
		change_column :question_answers, :result_id, :integer, null: true
	end

end
