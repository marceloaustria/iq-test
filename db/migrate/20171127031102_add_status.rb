class AddStatus < ActiveRecord::Migration
	def self.up
		add_column :courses, :status, :string, :default => "active"
		add_column :years, :status, :string, :default => "active"
		add_column :question_sections, :status, :string, :default => "active"
	end
	def self.down
		remove_column :courses, :status
		remove_column :years, :status
		remove_column :question_sections, :status
	end

end
