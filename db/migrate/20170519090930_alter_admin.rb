class AlterAdmin < ActiveRecord::Migration
	def self.up
		add_column :admins, :first_name, :string
		add_column :admins, :last_name, :string
		add_column :admins, :avatar, :string
	end
	def self.down
		remove_column :admins, :first_name
		remove_column :admins, :last_name
		remove_column :admins, :avatar
	end
end
