class UserTestStatus < ActiveRecord::Migration
	def self.up
		add_column :user_tests, :status, :string, :default => "active"
	end
	def self.down
		remove_column :user_tests, :status
	end
end
