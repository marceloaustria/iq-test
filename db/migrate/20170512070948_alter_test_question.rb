class AlterTestQuestion < ActiveRecord::Migration
	def self.up
		add_column :test_questions, :question_section_id, :integer
		
	end
	def self.down
		remove_column :test_questions, :question_section_id
	end
end
