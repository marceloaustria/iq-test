class Course < ActiveRecord::Migration
  def change
    create_table :courses do |t|
      t.string :course_name , limit: 1000
      t.timestamps null: false
    end
  end
end
