class Year < ActiveRecord::Migration
  def change
    create_table :years do |t|
      t.string :year_name , limit: 1000
      t.timestamps null: false
    end
  end
end
