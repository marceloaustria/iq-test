class AddResultIdUserPrepareTest < ActiveRecord::Migration
	def self.up
		change_column :user_prepare_tests, :result, :integer
		rename_column :user_prepare_tests, :result, :result_id
	end
	def self.down
		rename_column :user_prepare_tests, :result_id, :result
		change_column :user_prepare_tests, :result, :string
	end
end
