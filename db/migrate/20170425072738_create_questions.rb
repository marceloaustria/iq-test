class CreateQuestions < ActiveRecord::Migration
  def change
    create_table :questions do |t|
      t.string :question_name , limit: 1000
      t.integer :question_type_id
      t.integer :question_section_id
      t.integer :question_points
      t.string :status, limit: 10

      t.timestamps null: false
    end
  end
end
