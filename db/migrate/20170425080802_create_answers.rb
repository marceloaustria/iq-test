class CreateAnswers < ActiveRecord::Migration
  def change
    create_table :answers do |t|
      t.integer :question_id
      t.integer :answer_type_id
      t.string :answer_name

      t.timestamps null: false
    end
  end
end
