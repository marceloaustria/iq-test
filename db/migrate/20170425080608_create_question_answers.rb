class CreateQuestionAnswers < ActiveRecord::Migration
  def change
    create_table :question_answers do |t|
      t.integer :question_id
      t.string :question_answer_name
      t.integer :answer_result

      t.timestamps null: false
    end
  end
end
