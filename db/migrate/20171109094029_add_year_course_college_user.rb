class AddYearCourseCollegeUser < ActiveRecord::Migration
	def self.up
		add_column :users, :year, :string
		add_column :users, :course, :string

	end
	def self.down
		remove_column :users, :year
		remove_column :users, :course
	end
end
