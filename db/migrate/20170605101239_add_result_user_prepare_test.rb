class AddResultUserPrepareTest < ActiveRecord::Migration
	def self.up
		add_column :user_prepare_tests, :result, :string
		change_column :user_tests, :user_prepare_test_id, :integer
	end
	def self.down
		remove_column :user_prepare_tests, :result
		change_column :user_tests, :user_prepare_test_id, :string
	end
end