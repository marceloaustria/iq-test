class DropTable < ActiveRecord::Migration
  def up
    drop_table :answers
    drop_table :answer_types
    drop_table :question_types
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end

