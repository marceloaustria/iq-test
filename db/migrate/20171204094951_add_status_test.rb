class AddStatusTest < ActiveRecord::Migration
	def self.up
		add_column :tests, :status, :string, :default => "active"
	end
	def self.down
		remove_column :tests, :status
	end
end
